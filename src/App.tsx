import React from 'react';
// Context
import { ContextProvider } from './context/components/Context';
// Components
import BottomContent from './components/ui/layout/BottomContent';
import TopContent from './components/ui/layout/TopContent';
// Bootstrap
import Container from 'react-bootstrap/Container';
// Styles
import './App.scss';

function App() {
  return (
    <div className="app h-100">
      <ContextProvider>
        <Container className="app-container">
          <div className="app-top-content">
            <TopContent />
          </div>
          <div className="app-bottom-content shadow-sm">
            <BottomContent />
          </div>
        </Container>
      </ContextProvider>
    </div>
  );
}

export default App;
