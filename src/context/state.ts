// types
import { ResponseType } from '../api/types';

export type StateType = {
  responseData: null | ResponseType;
  requestInProcess: boolean;
}

// Values (default)
export const StateDefault: StateType = {
  requestInProcess: true,
  responseData: null,
};
