import { StateType } from './state';

const reducer = (
  state: StateType,
  action: { type: string; value: any },
): StateType => {
  switch (action.type) {
    case 'SET_REQUEST_IN_PROCESS': {
      return {
        ...state,
        requestInProcess: action.value
      };
    }
    case 'SET_RESPONSE': {
      return {
        ...state,
        responseData: action.value
      };
    }
    default:
      return state;
  }
};

export default reducer;
