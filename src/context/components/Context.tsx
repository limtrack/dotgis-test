import React, { useReducer } from 'react';
import { StateDefault, StateType } from '../state';
import reducer from '../reducer';

type ContextType = {
  state: StateType;
  dispatch: any;
};

export const Context = React.createContext<ContextType>({
  state: StateDefault,
  dispatch: null,
});

export const ContextProvider: React.FC = (props: any): JSX.Element => {
  const [state, dispatch] = useReducer(reducer, StateDefault);
  return (
    <Context.Provider value={{ state, dispatch }}>
      {props.children}
    </Context.Provider>
  );
};
