// Openweather
export const OWM_API_KEY = 'b60f2299c105010f6ec71c6be1ad45c5';
export const OWM_API_URL = 'http://api.openweathermap.org/data/2.5';
// Google map
export const GOOGLE_API_KEY = 'AIzaSyBWvjSUzb6JECy1PqziGuBMm0LlbQQhJfE';
export const DEFAULT_MAP_OPTIONS = {
  center: {
    lat: 40.4165000,
    lng: -3.7025600,
  },
  zoom: 5,
  mapTypeControl: false,
  streetViewControl: false,
  fullscreenControl: false,
}
