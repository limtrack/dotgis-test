import { useState, useEffect } from 'react';
// google maps
import { Loader } from 'google-maps';

export default function useGMLoader(apiKey: string): boolean {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (!window.google) {
      // Using an IIFE
      (async (): Promise<void> => {
        const loader = new Loader(apiKey);

        await loader.load();
        setLoaded(true);
      })();
    }
  }, [apiKey]);

  return loaded;
};
