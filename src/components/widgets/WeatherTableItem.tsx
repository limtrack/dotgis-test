import React from 'react';
// Types
import { ListDataType } from '../../api/types';
// Utils
import { secondsToDate } from '../../utils/date';
import { getIconUrl } from '../../utils/weather';
// Bootstrap
import Badge from 'react-bootstrap/Badge';

type Props = {
  data: null | ListDataType;
};

export default function WeatherTable({
  data = null,
}: Props): JSX.Element | null {
  return data ? (
    <tr>
      <td>
        {secondsToDate(data.dt, 'DD/MM - HH:mm')}
      </td>
      <td>
        <img
          alt={data.weather[0].description}
          src={getIconUrl(data.weather[0].icon)}
          width="30"
        />
        {data.weather[0].description}
      </td>
      <td>
        <Badge variant="danger">{data.main.temp_max}</Badge>{' '}
        <Badge variant="primary">{data.main.temp_min}</Badge>
      </td>
      <td>
        {data.main.humidity}
      </td>
      <td>
        {data.wind.speed}
      </td>
    </tr>
  ) : null;
}
