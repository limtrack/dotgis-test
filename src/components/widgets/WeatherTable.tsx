import React from 'react';
// Types
import { ListDataType } from '../../api/types';
// Components
import WidgetWrapper from '../ui/elements/WidgetWrapper';
import WeatherTableItem from './WeatherTableItem';
// Bootstrap
import Table from 'react-bootstrap/Table';

type Props = {
  data: null | ListDataType[];
};

export default function WeatherTable({
  data = null,
}: Props): JSX.Element | null {
  return data ? (
    <WidgetWrapper title="Datos brutos">
      <div className="table-responsive">
        <Table borderless hover size="sm">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Estado</th>
              <th>Temp (ºC)</th>
              <th>Humedad (%)</th>
              <th>Viento (m/s)</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
              return <WeatherTableItem data={item} key={item.dt} />;
            })}
          </tbody>
        </Table>
      </div>
    </WidgetWrapper>
  ) : null;
}
