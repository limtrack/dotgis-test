import React, { useRef, useEffect } from 'react';
// Types
import { ListDataType } from '../../api/types';
// Components
import WidgetWrapper from '../ui/elements/WidgetWrapper';
// Utils
import { secondsToDate } from '../../utils/date';
// Styles
import './WeatherChart.scss';

// Chart
const Chart = require('chart.js');

type Props = {
  data: null | ListDataType[];
};

export default function WeatherChart({
  data = null,
}: Props): JSX.Element | null {
  // wrapper map
  const wrapperChart = useRef<HTMLCanvasElement>(null);
  // data
  let chartData:any = null;

  if (data) {
    const xAxisValues = data.reduce((sumValues: string[], value: ListDataType) => {
      sumValues.push(secondsToDate(value.dt, 'DD/MM - HH:MM'));
      return sumValues;
    }, []);
    const yAxisTemperature = data.reduce((sumValues: number[], value: ListDataType) => {
      sumValues.push(value.main.temp);
      return sumValues;
    }, []);
    const yAxisHumidity = data.reduce((sumValues: number[], value: ListDataType) => {
      sumValues.push(value.main.humidity);
      return sumValues;
    }, []);

    chartData = {
      labels: xAxisValues,
      datasets: [
        {
          label: 'Temperatura',
          borderColor: '#F56384',
          backgroundColor: '#F56384',
          fill: false,
          data: yAxisTemperature,
          yAxisID: 'y-axis-1',
        },
        {
          label: 'Humedad',
          borderColor: '#39A2EB',
          backgroundColor: '#39A2EB',
          fill: false,
          data: yAxisHumidity,
          yAxisID: 'y-axis-2',
        },
      ],
    };
  }

  // Load chart
  useEffect(() => {
    if (wrapperChart.current && chartData) {
      Chart.Line(wrapperChart.current.getContext('2d'), {
        data: chartData,
        options: {
          responsive: true,
          hoverMode: 'index',
          stacked: false,
          scales: {
            yAxes: [
              {
                type: 'linear',
                display: true,
                position: 'left',
                id: 'y-axis-1',
              },
              {
                type: 'linear',
                display: true,
                position: 'right',
                id: 'y-axis-2',
                gridLines: {
                  drawOnChartArea: false,
                },
              }
            ],
          }
        }
      });
    }
  }, [wrapperChart, chartData]);

  return data ? (
    <WidgetWrapper title="Grafica evolución">
      <div className="chart">
        <canvas ref={wrapperChart}></canvas>
      </div>
    </WidgetWrapper>
  ) : null;
}
