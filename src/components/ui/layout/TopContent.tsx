import React, { useContext } from 'react';
// Context
import { Context } from '../../../context/components/Context';
// API
import { getDataByCoordinate } from '../../../api';
// Components
import WeatherMap from '../elements/WeatherMap';
import TopTitle from '../elements/TopTitle';
// Styles
import './TopContent.scss';

export default function TopContent(): JSX.Element {
  const { dispatch } = useContext(Context);
  // request loading
  const toggleLoading = (toggle: boolean): void => {
    dispatch({
      type: 'SET_REQUEST_IN_PROCESS',
      value: toggle,
    });
  };
  // API
  const handleClickCoordinate = async (coordinate: {
    lat: number;
    lng: number;
  }): Promise<any> => {
    // show loading
    toggleLoading(true);
    // save response
    dispatch({
      type: 'SET_RESPONSE',
      value: await getDataByCoordinate(coordinate),
    });
    // hide loading
    toggleLoading(false);
  };

  return (
    <div className="top-content">
      <TopTitle />
      <WeatherMap onClickCoordinate={handleClickCoordinate} />
    </div>
  );
}
