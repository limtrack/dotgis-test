import React, { useContext } from 'react';
// Context
import { Context } from '../../../context/components/Context';
// Components
import MySpinner from '../elements/MySpinner';
import WeatherChart from '../../widgets/WeatherChart';
import WeatherTable from '../../widgets/WeatherTable';
// Styles
import './BottomContent.scss';

const get = require('lodash.get');

export default function MainContent(): JSX.Element {
  const { state } = useContext(Context);
  const listData = get(state, 'responseData.list', null);

  return (
    <div className="bottom-content">
      {/* LOADING */}
      {state.requestInProcess && <MySpinner animation="border" />}
      {/* CONTENT */}
      {!state.requestInProcess && state.responseData && (
        <>
          <WeatherChart data={listData} />
          <WeatherTable data={listData} />
        </>
      )}
      {/* SIN DATOS */}
      {!state.requestInProcess && !state.responseData && 'Sin datos'}
    </div>
  );
}
