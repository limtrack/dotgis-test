import React, { useRef, useState, useEffect, useMemo } from 'react';
// Config
import { GOOGLE_API_KEY, DEFAULT_MAP_OPTIONS } from '../../../config';
// Utils
import {
  loadMap,
  addEventMap,
  removeEventMap,
  addMarker,
  removeMarker,
} from '../../../utils/map';
// Hooks
import useGMLoader from '../../../hooks/useGMLoader';
// Components
import MySpinner from '../elements/MySpinner';
// Styles
import './WeatherMap.scss';

let mapMarker: any = null;

type Props = {
  initialCoordinates?: {
    lat: number;
    lng: number;
  };
  onClickCoordinate?: (coordinate: any) => void;
};

const WeatherMap = ({
  initialCoordinates = DEFAULT_MAP_OPTIONS.center,
  onClickCoordinate,
}: Props): JSX.Element => {
  // wrapper map
  const wrapperMap = useRef<HTMLDivElement>(null);
  // state
  const [gMap, setGMap] = useState<any>(null);
  const isLoadedGM = useGMLoader(GOOGLE_API_KEY);
  // Memo
  const onClickCoordinateMemo = useMemo(() => {
    return onClickCoordinate;
  }, []);

  // Load map
  useEffect(() => {
    if (isLoadedGM) {
      const map = loadMap(wrapperMap.current as Element);
      // add initial mark
      if (initialCoordinates) {
        mapMarker = addMarker(map, initialCoordinates);
        typeof onClickCoordinateMemo === 'function' &&
          onClickCoordinateMemo(initialCoordinates);
      }
      setGMap(map);
    }
  }, [isLoadedGM, initialCoordinates, onClickCoordinateMemo]);

  // Add events
  useEffect(() => {
    let gEvent: any = null;

    if (isLoadedGM && gMap) {
      gEvent = addEventMap(gMap, 'click', (e: any) => {
        const coordinate = { lat: e.latLng.lat(), lng: e.latLng.lng() };
        // remove & add new mark
        removeMarker(mapMarker);
        mapMarker = addMarker(gMap, coordinate);
        typeof onClickCoordinateMemo === 'function' &&
          onClickCoordinateMemo(coordinate);
      });
    }

    return () => {
      removeEventMap(gEvent);
    };
  }, [isLoadedGM, gMap, onClickCoordinateMemo]);

  return isLoadedGM ? (
    <div ref={wrapperMap} className="weather-map"></div>
  ) : (
    <MySpinner animation="border" />
  );
};

export default React.memo(WeatherMap);
