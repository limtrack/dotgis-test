import React from 'react';
// Bootstrap
import Spinner, { SpinnerProps } from 'react-bootstrap/Spinner';
// Styles
import './MySpinner.scss';

type Props = {
  textLoading?: string;
} & SpinnerProps;

export default function AppSpinner({
  textLoading = 'Loading',
  animation = 'border',
  ...params
}: Props): JSX.Element {
  return (
    <div className="my-spinner-wrapper">
      <Spinner
        className="my-spinner-loader"
        animation={animation}
        role="status"
        {...params}
      >
        <span className="sr-only">{textLoading}</span>
      </Spinner>
      <span className="my-spinner-text">{textLoading}</span>
    </div>
  );
}
