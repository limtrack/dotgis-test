import React from 'react';
// Styles
import './WidgetWrapper.scss';

type Props = {
  title?: string;
  children: React.ReactNode;
};

export default function WidgetWrapper({
  title = 'Title widget',
  children,
}: Props): JSX.Element {
  return (
    <div className="widget">
      <h6 className="widget-title h6">{title}</h6>
      <div className="widget-content">{children}</div>
    </div>
  );
}
