import React, { useContext } from 'react';
// Types
import { CityType } from '../../../api/types';
// Context
import { Context } from '../../../context/components/Context';
// Utils
import { secondsToDate } from '../../../utils/date';
// Styles
import './TopTitle.scss';

const get = require('lodash.get');

export default function TopTitle(): JSX.Element | null {
  const { state } = useContext(Context);
  const cityData: CityType = get(state, 'responseData.city', null);

  return cityData ? (
    <div className="top-title">
      <h1 className="h1 top-title__name">
        {cityData.name}
        <small className="text-muted">(Predicción a 5 días y cada 3 horas)</small>
      </h1>
      <div className="top-title__info">
        <span>
          <b>Latitud / Longitud</b>
          {cityData.coord.lat} / {cityData.coord.lon}
        </span>
        <span>
          <b>Amanece / Puesta de sol</b>
          {secondsToDate(cityData.sunrise, 'HH:mm')} / {secondsToDate(cityData.sunset, 'HH:mm')}
        </span>
        <span>
          <b>Zona horaria</b>
          {cityData.timezone}
        </span>
      </div>
      
    </div>
  ) : null;
}
