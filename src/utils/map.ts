// Config
import { DEFAULT_MAP_OPTIONS } from '../config';

/**
 * Load map
 */
export function loadMap(element: Element, options = {}): any {
  const mapOptions = Object.assign(options, DEFAULT_MAP_OPTIONS);

  return new google.maps.Map(element, {
    center: mapOptions.center,
    zoom: mapOptions.zoom,
    mapTypeControl: mapOptions.mapTypeControl,
    streetViewControl: mapOptions.streetViewControl,
    fullscreenControl: mapOptions.fullscreenControl,
  });
}

/**
 * Add event to map
 */
export function addEventMap(
  map: any,
  event: string,
  fn: (e: any) => void
): any {
  return map && event && fn
    ? google.maps.event.addListener(map, event, (e) => {
        fn(e);
      })
    : null;
}

/**
 * Remove map event
 */
export function removeEventMap(event: any): void {
  event && google.maps.event.removeListener(event);
}

/**
 * Add marker to map
 */
export function addMarker(map: any, location: any): any {
  return map && location
    ? new google.maps.Marker({
        position: location,
        map,
      })
    : null;
}

/**
 * Remove map marker
 */
export function removeMarker(marker: any): void {
  marker && marker.setMap(null);
}
