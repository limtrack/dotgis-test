export function getIconUrl(icon: string, size = ''): string {
  return `http://openweathermap.org/img/wn/${icon}${size}.png`
}

