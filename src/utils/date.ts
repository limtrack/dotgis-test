import moment from 'moment';

// define
moment.locale('es');

export function secondsToDate(seconds: number, format = 'LLLL'): string {
  return moment(seconds*1000).format(format);
}

