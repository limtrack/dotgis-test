// Config
import { OWM_API_KEY, OWM_API_URL } from '../config';
// Types
import { CoordinateType, ApiOptions } from './types';

/**
 * Main function to get data from server
 */
async function getDataFromOpenWeather(
  // { type = 'hourly', query = '', others = { units: 'metric', lang: 'es' } } = {} as ApiOptions
  { query = '', others = { units: 'metric', lang: 'es' } } = {} as ApiOptions
): Promise<any> {
  const otherOptions = Object.entries(others)
    .reduce((sumOptions: string[], option) => {
      sumOptions.push(`${option[0]}=${option[1]}`);
      return sumOptions;
    }, [])
    .join('&');

  return await fetch(
    `${OWM_API_URL}/forecast?appid=${OWM_API_KEY}&${query}&${otherOptions}`
  ).then((response) => response.json());
}

/**
 * get data (weather or forecast) by coordinates
 */
export async function getDataByCoordinate({
  lat,
  lng,
}: CoordinateType): Promise<any> {
  if (!lat || !lng) {
    throw new Error('You must indicate a coordinate');
  }
  const query = `lat=${lat}&lon=${lng}`;

  return await getDataFromOpenWeather({ query });
}
