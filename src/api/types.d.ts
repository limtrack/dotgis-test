export type UnitsType = undefined | 'imperial' | 'metric';

export type CoordinateType = {
  lat: null | number;
  lng: null | number;
};

export type OthersType = {
  units?: UnitsType;
  lang?: string;
};

export type ApiOptions = {
  type?: 'hourly';
  query: string;
  others?: OthersType;
};

export type CityType = {
  id: number;
  name: string;
  coord: {
    lat: number;
    lon: number;
  };
  country: string;
  timezone: number;
  sunrise: number;
  sunset: number;
};

export type ListDataType = {
  dt: number;
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    sea_level: number;
    grnd_level: number;
    humidity: number;
    temp_kf: number;
  };
  weather: {
    id: number;
    main: string;
    description: string;
    icon: string;
  }[];
  clouds: {
    all: number;
  };
  wind: {
    speed: number;
    deg: number;
  };
  sys: {
    pod: string;
  };
  dt_txt: string;
};

export type ResponseType = {
  responseData: {
    cod: string;
    message: string | number;
    cnt: number;
    city: CityType;
    list: ListDataType[];
  };
};
